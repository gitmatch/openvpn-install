#!/usr/bin/env bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH
# sudo passwd root
# su
# wget https://bitbucket.org/gitmatch/openvpn-install/downloads/centos7-server-install.sh -O centos7-server-install.sh && bash centos7-server-install.sh

#下载代码配置githooks================================= start
function install_src_git()
{
 #echo $1,$2;
	#新建apache .ssh目录
	
	mkdir /usr/share/httpd/.ssh
	#修改.ssh目录权限
	chown -R apache:apache /usr/share/httpd/.ssh
	#生成apache公钥
	sudo -u apache ssh-keygen -t rsa
	echo "================id_rsa.pub start================="
	cat /usr/share/httpd/.ssh/id_rsa.pub 
	#cat /root/.ssh/id_rsa.pub
	echo "================id_rsa.pub end================="
	echo "请将id_rsa秘钥添加到bitbuket后台，"
	read -p "输入y继续: " next
	#安装git
	yum install -y git;
	mkdir /opt/aws-server;
	#修改代码提交权限
	chown -R apache:apache /opt/aws-server;
	# if cat '/etc/sudoers' | grep "NOPASSWD:/usr/bin/git" > /dev/null
	# then
		# echo "sudoers已存在"
	# else
		# #chmod 777 /etc/sudoers
		# echo "apache     ALL=(ALL)       NOPASSWD:/usr/bin/git" >> "/etc/sudoers"
		# #chmod 440 /etc/sudoers
	# fi

	#下载代码
	sudo -u apache cd /opt;sudo -u apache git clone git@bitbucket.org:gitmatch/aws-server.git;
	
	# if cat '/etc/rc.d/rc.local' | grep "git_socket_hooks" > /dev/null
	# then
		# echo "git_socket_hooks开机启动已存在"
	# else
		# echo "git_socket_hooks添加开机启动"
		# chmod +x /etc/rc.d/rc.local
		# echo "/usr/bin/php /opt/git_socket_hooks.php>/dev/null&" >> /etc/rc.d/rc.local
		
	# fi
	#/usr/bin/php /opt/aws-server/git_socket_hooks.php &

}

#下载代码配置githooks=================================end


#php56安装================================= start
function install_php()
{
	rpm -Uvh https://mirror.webtatic.com/yum/el7/epel-release.rpm
	rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
	yum install -y php55w-fpm php55w php55w-cli php55w-common php55w-gd php55w-ldap php55w-mbstring php55w-mcrypt php55w-mysql php55w-pdo php55w-posix php55w-pecl-redis
	#yum install -y php56w-fpm php56w php56w-cli php56w-common php56w-gd php56w-ldap php56w-mbstring php56w-mcrypt php56w-mysql php56w-pdo php56w-posix php56w-pecl-redis
	systemctl enable php-fpm
	#解决php-redis无法使用问题
	#/usr/sbin/setsebool httpd_can_network_connect=1
	#/usr/sbin/setsebool -P httpd_can_network_connect 1
	#关闭Selinux
	sed -i '/SELINUX/{s/enforcing/disabled/}' /etc/selinux/config
	#修改PHP上传大小
	sed -i '/upload_max_filesize/{s/2M/20M/}' /etc/php.ini

}

#php56安装================================= end
#安装nginx================================= start
function install_nginx()
{
	
	rpm -Uvh http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm
	yum install -y nginx
	wget https://bitbucket.org/gitmatch/openvpn-install/downloads/nginx.conf -O /etc/nginx/nginx.conf
	wget https://bitbucket.org/gitmatch/openvpn-install/downloads/poduu.conf -O /etc/nginx/conf.d/default.conf
	systemctl enable nginx.service
}
#安装nginx================================= end

#安装mysql================================= start
function install_mysql()
{
	rpm -Uvh http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm 
	yum install mysql-server
	systemctl enable mysqld
	systemctl start mysqld
}
#安装mysql================================= end


while :
	do
	clear
		echo "选择安装服务"
		echo "   1) 搭建代码服务器（php55,php-fpm,nginx,git）"
		echo "   2) 搭建Mysql服务器"
		echo "   3) 搭建Redis服务器"
		echo "   4) 退出"
		read -p "选着数字然后按回车 [1-4]: " option
		case $option in
			1) 
				yum remove -y php* httpd*;
				echo "开始安装php";
				install_php;
				echo "开始安装nginx";
				install_nginx;
				
				echo "开始安装代码和git";
				install_src_git;
				
				systemctl start php-fpm;
				systemctl start nginx.service;
				echo "代码服务器搭建完成"
				echo "请重启服务器"
			exit
			;;
			2)
				echo "开始搭建mysql服务器";
				install_mysql;
				echo "Mysql服务器搭建完成"
			exit
			;;
			3) 
			echo ""
			read -p "你确定要删除 OpenVPN 吗? [y/n]: " -e -i n REMOVE
			if [[ "$REMOVE" = 'y' ]]; then
				
				echo "OpenVPN removed!"
			else
				echo ""
				echo "Removal aborted!"
			fi
			exit
			;;
			4) exit;;
		esac
	done















